def find_start_end_section(data, start_section, end_section=None):
    start = next((index for index, row in enumerate(data) if start_section in row), None)
    end = next((index for index, row in enumerate(data) if end_section is not None and end_section in row), None)
    return start, end


def extract_section_rows(data, start_section, end_section=None, include_start=False):
    start, end = find_start_end_section(data, start_section, end_section)
    end = len(data) if end is None else end
    # need to add "something" that allows me to say whether or not I want to include "start" line as part of my result
    if include_start:
        return [(index, row) for index, row in enumerate(data) if row != [] and index >= start and index < end]
    
    return [(index, row) for index, row in enumerate(data) if row != [] and index > start and index < end]